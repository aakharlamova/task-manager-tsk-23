package ru.kharlamova.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kharlamova.tm.api.service.ServiceLocator;
import ru.kharlamova.tm.enumerated.Role;

public abstract class AbstractCommand {

    @Nullable
    protected ServiceLocator serviceLocator;

    public void setServiceLocator(@NotNull ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    public Role[] roles() {
        return null;
    }

    @Nullable
    public abstract String arg();

    @NotNull
    public abstract String name();

    @Nullable
    public abstract String description();

    public abstract void execute();

}
