package ru.kharlamova.tm.exception.entity;

import ru.kharlamova.tm.exception.AbstractException;

public class TaskNotFoundException extends AbstractException {

    public TaskNotFoundException() {
        super("Error! Task not found.");
    }

}
