package ru.kharlamova.tm.exception;

import org.jetbrains.annotations.Nullable;

public abstract class AbstractException extends RuntimeException {

    protected String message;

    protected AbstractException(@Nullable final String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

}
