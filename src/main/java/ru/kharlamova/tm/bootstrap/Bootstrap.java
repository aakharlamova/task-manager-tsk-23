package ru.kharlamova.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import ru.kharlamova.tm.api.repository.ICommandRepository;
import ru.kharlamova.tm.api.repository.IProjectRepository;
import ru.kharlamova.tm.api.repository.ITaskRepository;
import ru.kharlamova.tm.api.repository.IUserRepository;
import ru.kharlamova.tm.api.service.*;
import ru.kharlamova.tm.command.AbstractCommand;
import ru.kharlamova.tm.command.authorization.*;
import ru.kharlamova.tm.command.project.*;
import ru.kharlamova.tm.command.system.*;
import ru.kharlamova.tm.command.task.*;
import ru.kharlamova.tm.command.authorization.UserByLoginLockCommand;
import ru.kharlamova.tm.command.authorization.UserByLoginRemoveCommand;
import ru.kharlamova.tm.command.authorization.UserByLoginUnlockCommand;
import ru.kharlamova.tm.enumerated.Role;
import ru.kharlamova.tm.enumerated.Status;
import ru.kharlamova.tm.exception.system.UnknownArgumentException;
import ru.kharlamova.tm.exception.system.UnknownCommandException;
import ru.kharlamova.tm.repository.CommandRepository;
import ru.kharlamova.tm.repository.ProjectRepository;
import ru.kharlamova.tm.repository.TaskRepository;
import ru.kharlamova.tm.repository.UserRepository;
import ru.kharlamova.tm.service.*;
import ru.kharlamova.tm.util.TerminalUtil;

import java.util.Optional;

public class Bootstrap implements ServiceLocator {

    @NotNull private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull  private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull  private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull  private  final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    @NotNull  private final ILoggerService loggerService = new LoggerService();

    @NotNull  private final IUserRepository userRepository = new UserRepository();

    @NotNull  private final IUserService userService = new UserService(userRepository);

    @NotNull  private final IAuthService authService = new AuthService(userService);

    private void initUsers() {
        userService.create("test","test","test@test.ru");
        userService.create("admin","admin", Role.ADMIN);
    }

    private void initData() {
        taskService.add("1-0","TEST1", "123").setStatus(Status.COMPLETE);
        taskService.add("1-1","TEST2","123").setStatus(Status.COMPLETE);
        taskService.add("1-2","TEST3","000").setStatus(Status.IN_PROGRESS);
        taskService.add("1-3","TEST4","desc").setStatus(Status.NOT_STARTED);
        projectService.add("2-0", "Project1","desc").setStatus(Status.COMPLETE);
        projectService.add("2-1", "Project12","abc").setStatus(Status.IN_PROGRESS);
        projectService.add("2-2", "Project3", "test").setStatus(Status.IN_PROGRESS);
        projectService.add("2-3", "Project4","exc").setStatus(Status.NOT_STARTED);
    }

    {
        registry(new UserLogInCommand());
        registry(new UserLogOutCommand());
        registry(new UserRegistryCommand());
        registry(new UserChangePasswordCommand());
        registry(new UserUpdateProfileCommand());
        registry(new UserViewProfileCommand());
        registry(new UserByLoginLockCommand());
        registry(new UserByLoginRemoveCommand());
        registry(new UserByLoginUnlockCommand());

        registry(new AboutCommand());
        registry(new ArgumentsListCommand());
        registry(new CommandsListCommand());
        registry(new ExitCommand());
        registry(new HelpCommand());
        registry(new SystemInfoCommand());
        registry(new VersionCommand());

        registry(new ProjectClearCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectShowAllCommand());
        registry(new ProjectByIdChangeStatusCommand());
        registry(new ProjectByIdFinishCommand());
        registry(new ProjectByIdRemoveCommand());
        registry(new ProjectByIdRemoveWithTasksCommand());
        registry(new ProjectByIdShowCommand());
        registry(new ProjectByIdStartCommand());
        registry(new ProjectByIdUpdateCommand());
        registry(new ProjectByIndexChangeStatusCommand());
        registry(new ProjectByIndexFinishCommand());
        registry(new ProjectByIndexRemoveCommand());
        registry(new ProjectByIndexShowCommand());
        registry(new ProjectByIndexStartCommand());
        registry(new ProjectByIndexUpdateCommand());
        registry(new ProjectByNameChangeStatusCommand());
        registry(new ProjectByNameFinishCommand());
        registry(new ProjectByNameRemoveCommand());
        registry(new ProjectByNameShowCommand());
        registry(new ProjectByNameStartCommand());

        registry(new TaskClearCommand());
        registry(new TaskCreateCommand());
        registry(new TaskShowAllCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new TaskBindToProjectCommand());
        registry(new TaskByIdChangeStatusCommand());
        registry(new TaskByIdFinishCommand());
        registry(new TaskByIdRemoveCommand());
        registry(new TaskByIdShowCommand());
        registry(new TaskByIdStartCommand());
        registry(new TaskByIdUpdateCommand());
        registry(new TaskByIndexChangeStatusCommand());
        registry(new TaskByIndexFinishCommand());
        registry(new TaskByIndexRemoveCommand());
        registry(new TaskByIndexShowCommand());
        registry(new TaskByIndexStartCommand());
        registry(new TaskByIndexUpdateCommand());
        registry(new TaskByNameChangeStatusCommand());
        registry(new TaskByNameFinishCommand());
        registry(new TaskByNameRemoveCommand());
        registry(new TaskByNameShowCommand());
        registry(new TaskByNameStartCommand());
        registry(new TaskByProjectIdShowAllCommand());
    }

    public void run(final String... args) {
        loggerService.debug("TEST");
        loggerService.info("*** WELCOME TO TASK MANAGER ***");
        if (parseArgs(args)) System.exit(0);
        initUsers();
        initData();
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = TerminalUtil.nextLine();
            loggerService.command(command);
            try {
                parseCommand(command);
                System.err.println("[OK]");
            } catch (final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    private void registry(final AbstractCommand command) {
        if (!Optional.ofNullable(command).isPresent()) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void parseCommand(final String cmd) {
        if (!Optional.ofNullable(cmd).isPresent()) return;
        final AbstractCommand command= commandService.getCommandByName(cmd);
        if (!Optional.ofNullable(command).isPresent()) throw new UnknownCommandException(cmd);
        final Role[] roles = command.roles();
        authService.checkRoles(roles);
        command.execute();
    }

    public void parseArg(String arg) {
        if (!Optional.ofNullable(arg).isPresent()) return;
        final AbstractCommand command = commandService.getCommandByArg(arg);
        if (!Optional.ofNullable(command).isPresent()) throw new UnknownArgumentException(arg);
        command.execute();
    }

    public boolean parseArgs(String[] args) {
        if (!Optional.ofNullable(args).isPresent() || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    @NotNull
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @NotNull
    public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull
    public ICommandService getCommandService() {
        return commandService;
    }

    @NotNull
    public IAuthService getAuthService() {
        return authService;
    }

    @NotNull
    public IUserService getUserService() {
        return userService;
    }

}
