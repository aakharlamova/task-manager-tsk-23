package ru.kharlamova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kharlamova.tm.enumerated.Role;
import ru.kharlamova.tm.model.User;

import java.util.Optional;

public interface IAuthService {

    @NotNull
    Optional<User> getUser();

    @NotNull
    String getUserId();

    void checkRoles(Role... roles);

    boolean isAuth();

    void logout();

    void login(@Nullable String login, @Nullable String password);

    void register(@Nullable String login, @Nullable String password, @Nullable String email);

}
