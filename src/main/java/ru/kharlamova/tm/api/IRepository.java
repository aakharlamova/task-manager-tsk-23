package ru.kharlamova.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kharlamova.tm.model.AbstractEntity;

import java.util.List;
import java.util.Optional;

public interface IRepository<E extends AbstractEntity> {

    @NotNull
    List<E> findAll();

    @NotNull
    E add(@NotNull E entity);

    @Nullable
    Optional<E> findById(@NotNull String id);

    @Nullable
    E removeById(@NotNull String id);

    void clear();

    void remove(@Nullable E entity);

}
